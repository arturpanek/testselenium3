package controllers;


import io.restassured.response.Response;
import specs.SpecStrategy;

import static io.restassured.RestAssured.given;

public class WeatherController extends BaseController {

    private static final String PATH = "/weather";

    public WeatherController(SpecStrategy specStrategy) {
        super(specStrategy);
    }

    public Response getWeather(String paramName, String paramValue) {
        return given().
                spec(spec).
                basePath(PATH).
                param(paramName, paramValue).
                when().
                get();
    }
}
