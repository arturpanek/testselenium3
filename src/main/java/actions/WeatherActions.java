package actions;


import controllers.WeatherController;
import io.restassured.response.Response;
import specs.DefaultSpecStrategy;

public class WeatherActions {

    private static WeatherController weatherController = new WeatherController(new DefaultSpecStrategy());

    public Response getWeatherForCity(String cityName) {
        return weatherController.getWeather("q", cityName);
    }

    public Response getWeatherForCityAndCountry(String cityName, String countryName) {
        return weatherController.getWeather("q", cityName + "," + countryName);
    }
}
