@weather
Feature: Get weather for specified city

  Scenario: Send request and verify status code
    When I get weather for city "Rzeszow"
    Then Returned status code should be 200
    And Country code should be "PL"
