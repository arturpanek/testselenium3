package steps;


import cucumber.api.java.en.Then;
import utils.ResponseHolder;

public class CommonSteps {

    private ResponseHolder responseHolder;

    public CommonSteps(ResponseHolder responseHolder) {
        this.responseHolder = responseHolder;
    }

    @Then("^Returned status code should be (\\d+)$$")
    public void returnedStatusCodeShouldBe(int statusCode) throws Throwable {
        responseHolder.getResponse().then().assertThat().
                statusCode(statusCode);
    }
}
