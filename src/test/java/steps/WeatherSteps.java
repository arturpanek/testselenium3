package steps;


import actions.WeatherActions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import domain.CurrentWeather;
import org.junit.Assert;
import utils.ResponseHelper;
import utils.ResponseHolder;

public class WeatherSteps {

    private static WeatherActions weatherActions = new WeatherActions();

    private ResponseHolder responseHolder;

    public WeatherSteps(ResponseHolder responseHolder) {
        this.responseHolder = responseHolder;
    }

    @When("^I get weather for city \"([^\"]*)\"$")
    public void iGetWeatherForCity(String cityName) throws Throwable {
        responseHolder.setResponse(weatherActions.getWeatherForCity(cityName));
    }

    @And("^Country code should be \"([^\"]*)\"$")
    public void countryCodeShouldBe(String countryCode) throws Throwable {
        CurrentWeather currentWeather = ResponseHelper.getObjectFromResponse(responseHolder.getResponse(), CurrentWeather.class);
        Assert.assertEquals("Country code is not correct", countryCode, currentWeather.getSys().getCountry());
    }
}
